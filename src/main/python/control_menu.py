#!/usr/bin/env python

import os
from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import Qt, Signal, Slot, QObject
from PySide2.QtWidgets import (QMenu, QComboBox, QGroupBox, QLabel,  QPushButton, QGridLayout,
                               QListWidget, QVBoxLayout, QHBoxLayout, QCheckBox, QFileDialog,
                               QLineEdit)
from pos_manager import PosController


class ControlTab(QtWidgets.QFrame):
    def __init__(self):
        super().__init__()
        self.create_grid()

    def create_grid(self):
        self.grid = QtWidgets.QGridLayout()
        self.grid.setAlignment(Qt.AlignTop | Qt.AlignLeft)


class layoutControl(ControlTab):
    def __init__(self, channels, nstacks = 5):
        super().__init__()
        self.channels = channels 
        self.nstacks = nstacks
        self.clippable = True if 'segoutlines' in channels else False
        self.categ_ns = {
            'channels': len(self.channels),
            'stacks': self.nstacks,
        }
        self.categs = ['channels', 'clipped', 'stacks']
        self.sorting = {
            'rowids': ['channels'],
            'colids': ['stacks']
        }

        axis_control = AxisControl()
        self.grid.addWidget(axis_control,0, 0, 1,1)

        channel_control = ChannelControl(channels, clip=self.clippable)
        self.grid.addWidget(channel_control, 0, 1, 1,1)

        self.set_layout()

    def set_layout(self):
        self.setLayout(self.grid)


class annotationControl(ControlTab):
    def __init__(self):
        super().__init__()
        self.outline_mode = ['BSpline']  #, 'Spline', 'Ellipse']

        self.emode_list = editModeList()
        self.omode_box = QComboBox()
        self.quick = quickAnnotationButtons()

        for mode in self.outline_mode:
            self.omode_box.addItem(mode)

        self.grid.addWidget(QLabel('Mode'), 0, 0, 1, 2)
        self.grid.addWidget(self.emode_list, 1, 0,
                            len(self.emode_list.edit_modes), 2)
        self.grid.addWidget(QLabel('Outline mode'), 0, 3, 1, 1)
        self.grid.addWidget(self.omode_box, 1, 3, 1, 1)
        self.grid.addWidget(self.quick,2,3,1,1)

        self.setLayout(self.grid)

class quickAnnotationButtons(QtWidgets.QGroupBox):
    def __init__(self):
        super().__init__()
        self.setTitle("Quick Annotation")
        self.layout = QVBoxLayout()
        self.save_next_btn = QPushButton('Save to file and continue')
        self.layout.addWidget(self.save_next_btn)
        self.setLayout(self.layout)

class editModeList(QListWidget):
    def __init__(self):
        super().__init__()
        self.setFixedSize(60, 100)

        # self.edit_modes = ['Add', 'Draw', 'Select', 'Handle', 'Remove', 'Copy']
        self.edit_modes = ['Add', 'Select', 'Handle', 'Remove', 'Copy']

        for mode in self.edit_modes:
            self.addItem(mode)

    @Slot(str)
    def set_edit_mode(self, edit_mode):
        #TODO add a check step
        self.setCurrentRow(self.edit_modes.index(edit_mode))


class IOControl(ControlTab):
    def __init__(self):
        super().__init__()

class VGroupBox(QGroupBox):
    '''
    Convenience class to pile widgets vertically
    '''
    def __init__(self, title):
        super().__init__(title)

        self.setTitle(title)
        self.layout = QVBoxLayout()
        self.layout.setAlignment(Qt.AlignTop | Qt.AlignLeft)

class AxisControl(VGroupBox):
    def __init__(self):
        super().__init__('Layers Orientation')
        levels = ['Channels', 'Stacks', 'Timepoints']
        colnames = ['x-axis', 'y-axis', 'Basic']

        self.button_groups = {}

        for level in levels:
            self.button_groups[level] = AxisBoxes(level)

        for group in self.button_groups.values():
            self.layout.addWidget(group)

        self.set_defaults()
        
        self.setLayout(self.layout)

    def set_defaults(self):
        self.button_groups['Channels'].x_axis.setChecked(True)
        self.button_groups['Stacks'].y_axis.setChecked(True)
        self.button_groups['Timepoints'].basic.setChecked(True)
    
class AxisBoxes(QGroupBox):
    def __init__(self, level):
        super().__init__(level)
        self.level = level

        layout = QHBoxLayout()

        self.x_axis = QCheckBox('x-axis')
        layout.addWidget(self.x_axis)

        self.y_axis = QCheckBox('y-axis')
        layout.addWidget(self.y_axis)

        if self.level != 'channel' or self.state != 'Basic':
            self.basic = QCheckBox('Basic') 
            layout.addWidget(self.basic)
        

        self.setLayout(layout)

class ChannelControl(VGroupBox):
    def __init__(self, channels, clip=None):
        super().__init__('Channels')

        self.channel_groups = {}

        for channel in channels:
            self.channel_groups[channel] = ChannelBoxes(channel, clip=clip)
            self.layout.addWidget(self.channel_groups[channel])

        self.setLayout(self.layout)
        
class ChannelBoxes(QGroupBox):
    def __init__(self, channel, clip):
        super().__init__(channel)
        self.channel = channel
        self.clip = clip

        self.active_button = QCheckBox('Active')
        layout = QHBoxLayout()
        layout.addWidget(self.active_button)

        if self.clip and self.channel != 'segoutlines':
            self.clip_box = QCheckBox('Clip')
            layout.addWidget(self.clip_box)

        self.setLayout(layout)

    
class BareGroupBox(QGroupBox):
    '''
    Text-less groupbox for use in a matrix.
    Retains information of the corresponding row
    '''
    def __init__(self, level):
        super().__init__()
        self.level = level

class BareRadioButton(QtWidgets.QRadioButton):
    '''
    Text-less button for use in a matrix.
    Retains information of position in matrix
    '''
    def __init__(self, level, state):
        super().__init__()
        self.level = level
        self.state = state

class tabsHolder(QtWidgets.QTabWidget):
    def __init__(self, channels):
        super().__init__()

        self.tabs = {
            'I/O': ioFrame(),
            'Annotation': annotationControl(),
            'Views': layoutControl(channels)
        }

        for name, tab in self.tabs.items():
            self.addTab(tab, name)

        self.connect_signals()


    @Slot()
    def setAnnotationTab(self):
        self.setCurrentWidget(self.tabs['Annotation'])

    def connect_signals(self):
        self.tabs['I/O'].signals.folder_loaded.connect(self.setAnnotationTab)


class menuFrame(QGroupBox):
    def __init__(self):
        super().__init__()

        self.layout = QtWidgets.QVBoxLayout()

        # TODO make this agnostic
        # metadata_fields = ['position', 'trap', 'tp', 'cell_id']
        self.pos_control = PosController()
        self.layout.addWidget(self.pos_control.location_frame)

        self.tabs_holder = tabsHolder(['GFP', 'segoutlines'])
        # self.setFrameStyle(QtWidgets.QFrame.StyledPanel)
        self.layout.addWidget(self.tabs_holder)
        self.connect_signals()

        self.setLayout(self.layout)

    def connect_signals(self):
        self.tabs_holder.tabs['I/O'].signals.folder_loaded.connect(
            self.pos_control.load_input)

class ioFrame(QtWidgets.QGroupBox):
    def __init__(self, input_path=None, out_path=None):
        super().__init__()
        self.input_path = input_path
        self.out_path = out_path

        self.init_frame()

        self.setTitle("I/O Control")

    def init_frame(self):
        # Dialog widget
        # Input
        self.dialog_input_dir = QFileDialog()
        self.dialog_input_dir.setFileMode(QFileDialog.Directory)
        self.signals = ioSignals()

        # Output
        self.dialog_output_dir = QFileDialog()
        self.dialog_output_dir.setFileMode(QFileDialog.Directory)

        # Select folder
        self.button_browse = QPushButton()
        self.button_browse.setText("Find Folder")

        self.label_input = QLabel()
        self.label_input_title = QLabel()
        self.label_input_title.setText("Input folder:")

        # Save curated data
        # Annotation name
        self.annot_name = QLineEdit('feat_outlines')

        self.button_save = QPushButton()
        self.button_save.setText("Save annotations")

        # Connect buttons to functions
        self.button_browse.clicked.connect(self.get_input)
        self.button_save.clicked.connect(self.generate_outfile)

        self.layout= QGridLayout()

        self.layout.addWidget(self.button_browse, 0,0,1,1)
        self.layout.addWidget(self.button_save, 0,1,1,1)
        self.layout.addWidget(self.label_input_title, 1,0,1,2)
        self.layout.addWidget(self.label_input, 2,0,1,2)
        self.layout.addWidget(QLabel('Feature name:'), 3, 0, 1, 2)
        self.layout.addWidget(self.annot_name, 4,0,1,2)

        self.setLayout(self.layout)

    def get_input(self):
        self.input_dir = os.path.dirname(self.dialog_input_dir.getOpenFileName(self, 'Browse file or folder')[0]) 
        if self.input_dir:
            self.signals.folder_loaded.emit(self.input_dir)
            self.label_input.setText(self.input_dir)

    def generate_outfile(self):
        self.output_dir = self.dialog_output_dir.getExistingDirectory(self, 'Set annotation folder')
        self.signals.signal_str.emit(self.output_dir)

class ioSignals(QObject):
    signal_list = Signal(list)
    folder_loaded = Signal(str)
