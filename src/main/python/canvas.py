#!/usr/bin/env python
'''
Main class that is used to visualise and edit an image and classes used inside it.
'''
import copy
import numpy as np
from numpy import newaxis as nax

from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Qt, QObject, Signal, Slot, QPointF
from PySide2.QtWidgets import QGraphicsItem, QGraphicsScene, QGraphicsView, QGraphicsEllipseItem, QGraphicsPolygonItem
from PySide2.QtGui import QPen, QBrush, QColor, QPolygonF

from PIL.ImageQt import ImageQt
from skimage.io._plugins.pil_plugin import ndarray_to_pil

# from scipy import interpolate
from matplotlib import pyplot as plt
from matplotlib import colors 

from image_io import normalise_array
from geom_functions import get_handle_loc, get_new_params, get_handle_color, fit_ellipse
from geom_functions import bspline


class Canvas(QtWidgets.QGraphicsView):
    '''Manages a single image.'''
    start = None
    end = None
    item = None
    path = None

    dragged_handle = None

    def __init__(self, canvas_id, categs_dict, img_scale=4, img=None):
        super().__init__()
        # self.setFrameShape(QFrame.NoFrame);
        self.canvas_id = canvas_id
        self.cell = categs_dict.get('cell', None)
        self.stack = categs_dict.get('stack', None)
        self.channel = categs_dict.get('channel', None)
        self.img_scale = img_scale

        self.trackable_modes = ['Add', 'Draw']  # For manual mouse tracking

        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        self.setAcceptDrops(True)

        self.myscene = QtWidgets.QGraphicsScene()
        self.outlines = []


        self.setScene(self.myscene)

        self.last_outline = []
        self.prev_outlines = []

        self.outline_mode = None

        self.signals = canvasSignals()
        self.myscene.selectionChanged.connect(self.filter_sel_change)

    def filter_sel_change(self):
        sel_items = self.myscene.selectedItems()
        if self.edit_mode == 'Remove':
            for item in sel_items:
                if isinstance(item, Outline):
                    self.removeItem(item)
                    self.signals.removed_outlines.emit([x.outline_id for
                                                x in sel_items if getattr(x, 'outline_id')],
                                              self.canvas_id)
             
        elif self.edit_mode == 'Select' or 'Handle':
            if len(sel_items) == 1:
                sel_id = sel_items[0].outline_id
            else: 
                sel_id = None
            self.signals.new_selection.emit(sel_id)

    def set_edit_mode(self, edit_mode):
        if not hasattr(self, 'edit_mode'):
            self.edit_mode = 'Add'
        elif edit_mode != self.edit_mode:
            self.edit_mode = edit_mode
            self.signals.edit_mode_changed.emit(edit_mode)

    def set_outline_mode(self, outline_mode):
        if outline_mode != self.outline_mode:
            self.outline_mode = outline_mode
            self.signals.outline_mode_changed.emit(outline_mode)

    def update_image(self, img):
        print("Updating to new image of shape ", img.shape)
        xsize, ysize = img.shape[:2]
        self.setFixedSize(ysize * self.img_scale, xsize * self.img_scale)
        self.myscene.setSceneRect(
            QtCore.QRectF(0, 0, xsize * self.img_scale,
                          ysize * self.img_scale))
        qt_image = ImageQt(ndarray_to_pil(normalise_array(img)))
        self.pmap = self.myscene.addPixmap(QtGui.QPixmap.fromImage(qt_image))
        self.pmap.setScale(self.img_scale)
        self.pmap.setPos(0, 0)
                                                  
    def tabletEvent(self, tabletEvent):
        if self.edit_mode in self.trackable_modes:
            self.pen_x = tabletEvent.globalX()
            self.pen_y = tabletEvent.globalY()

            if tabletEvent.type() == QtGui.QTabletEvent.TabletPress:
                self.pen_is_down = True
                self.text = "TabletPress event"
                self.press_event(self.pen_x, self.pen_y)
            elif tabletEvent.type() == QtGui.QTabletEvent.TabletMove:
                self.pen_is_down = True
                self.text = "TabletMove event"
                self.move_event(self.pen_x, self.pen_y, self.pen_pressure)
            elif tabletEvent.type() == QtGui.QTabletEvent.TabletRelease:
                self.pen_is_down = False
                self.text = "TabletRelease event"
                self.release_event()
            self.text += " at x={0}, y={1}, pressure={2}%,".format(
                self.pen_x, self.pen_y, self.pen_pressure)
            if self.pen_is_down:
                self.text += " Pen is down."
            else:
                self.text += " Pen is up."

            tabletEvent.accept()
            self.signals.drawing_updated.emit(self.text)

    def press_pen(self, x, y):
        self.last_x = x
        self.last_y = x

        self.drawn_path = QtGui.QPainterPath()
        self.last_path = pathItem(self.img_scale)
        if self.edit_mode == 'Draw':
            self.myscene.addItem(self.last_path)
            self.start = self.mapToScene(x, y)
            self.drawn_path.moveTo(self.start)

    def move_pen(self, x, y):
        self.last_outline.append((x, y))
        if self.edit_mode == 'Draw':
            self.end = self.mapToScene(x, y)
            self.drawn_path.lineTo(self.end)
            self.last_path.setPath(self.drawn_path)
            self.start = self.end

        # Update the origin for next time.
        self.last_x = x
        self.last_y = y

    def press_event(self, x, y):
        if self.edit_mode in self.trackable_modes:
            self.press_pen(x, y)

    def move_event(self, x, y, pressure=None):
        self.text = "Mouse moved. Now at x={0}, y={1}".format(x, y)
        self.signals.drawing_updated.emit(self.text)

        if self.edit_mode in self.trackable_modes:
            self.move_pen(x, y)

    def mousePressEvent(self, e):
        if self.edit_mode in self.trackable_modes:
            self.press_event(e.x(), e.y())
        super(Canvas, self).mousePressEvent(e)

    def mouseMoveEvent(self, e):
        if self.edit_mode in self.trackable_modes:
            self.move_event(e.x(), e.y())
        super(Canvas, self).mouseMoveEvent(e)

    def mouseReleaseEvent(self, e):
        self.release_event(e.x(), e.y())
        super(Canvas, self).mouseReleaseEvent(e)

    def release_event(self, x, y):
        if self.edit_mode in self.trackable_modes:
            # if self.edit_mode == 'Draw':
            #     self.prev_outlines.append(self.last_outline)
            #     self.add_outline()
            #     # self.draw_outline_from_points(self.outline_mode, self.last_outline)
            #     self.removeItem(self.last_path)
            #     self.last_outline = []
            if self.edit_mode == 'Add':
                self.add_outline(x, y)
        # if self.edit_mode == 'Remove':
        #     print('try to remove')
        #     print(self.myscene.itemAt(QPointF(x,y)))
            # self.removeItem()
            # self.change_mode('Handle')

    def removeItem(self, item):
        self.myscene.removeItem(item)

    def change_mode(self, edit_mode):
        if edit_mode != self.edit_mode:
            self.edit_mode = edit_mode
                
            self.signals.edit_mode_changed.emit(edit_mode)


    # @Slot(float, float)
    def add_outline(self, x, y):
        self.signals.new_outline.emit(x,y)
        # if self.outline_mode == 'BSpline':
        # self.add_and_connect(BSpline(self.myscene, len(self.outlines), centre=[x, y]))
        # if emit:
        #     self.signals.updated_outline.emit(self.canvas_id,
        #                                   self.outlines[-1].outline_id) 

    @Slot(int, int)
    def update_polygon(self, c_id, o_id):
        idx = self.find_index(o_id)
        self.outlines[idx].fit_spline()
        self.outlines[idx].set_polygon()
        # self.signals.updated_outline.emit(
        #     self.canvas_id, outline_id)

    def set_selection(self, outline_id, draw_handles=False):
        # Draws the outline of selection and optionally the handles too
        self.selected_outline = outline_id
        if draw_handles:
            self.signals.handles_drawn.emit()
            self.outlines[outline_id].show_handles()
                                                  
    def find_index(self, o_id):
        return next((i for i, o in enumerate(self.outlines)
                     if o.outline_id == o_id), None)
     
    #TODO add movement sync

class Handle(QGraphicsEllipseItem):
    def __init__(self, outline_id, handle_id, x, y, rad=5, parent=None):
        super().__init__(x - rad, y - rad, 2 * rad, 2 * rad, parent=parent)
        self.outline_id = outline_id
        self.handle_id = handle_id
        self.parent = parent

        self.setFlags(QGraphicsItem.ItemIsMovable
                      | QGraphicsItem.ItemSendsGeometryChanges)

        # col = get_handle_color(handle_id) HOLD: Adjust this after adding siz/rot handle
        col = get_color(outline_id)
        self.setBrush(QBrush(QColor(col)))

        self.orig_x, self.orig_y = (x, y)
        self.signals = handleSignals()

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemPositionChange:
            self.signals.pos_changed.emit(self.parent.canvas.canvas_id,
                                          self.outline_id, self.handle_id,
                                          value.x(), value.y())
        return super().itemChange(change, value)  # Call super

class canvasSignals(QObject):
    edit_mode_changed = Signal(str)
    outline_mode_changed = Signal(str)
    new_outline = Signal(int, int) # Signal to indicate a new outline to add
    removed_outlines = Signal(list, int)
    drawing_updated = Signal(str)
    new_selection = Signal(int)
    deselected = Signal()
    handles_drawn = Signal()


class handleSignals(QObject):
    pos_changed = Signal(int, int, int, float, float) # canvas, outline, handle, dx, dy


class pathItem(QtWidgets.QGraphicsPathItem):
    def __init__(self, scale):
        super().__init__()
        pen = QtGui.QPen()
        pen.setColor('#5ebb49')
        pen.setWidth(0.5 * scale)
        self.setPen(pen)

class penItem(QPen):
    def __init__(self, width=1):
        super().__init__()
        self.setWidth(width)

        

class Outline(QGraphicsPolygonItem):
    def __init__(self,
                 outline_id,
                 canvas = None,
                 centre=None,
                 points=None,
                 parameters=None):
        super().__init__()
        self.outline_id = outline_id
        self.canvas = canvas

        self.centre = centre

        if points is not None:
            points = [np.array(points)[:, 0], np.array(points)[:, 1]]
        self.points = points
        

        self.parameters = [] if parameters is None else parameters
        self.handles = []
        self.dragged_handle = None
        self.selected = False

        # self.setFlags(QGraphicsItem.ItemIsSelectable | QGraphicsItem.ItemIsMovable)
        # self.setFlags(QGraphicsItem.ItemIsSelectable)

    def set_handles(self, handles_pos):
        self.handles = [
            Handle(self.outline_id, i, x, y, parent = self)
            for i, (x, y) in enumerate(handles_pos)
        ]

    def show_handles(self):
        for handle in self.handles:
            if handle not in scene.items():      
                self.scene.addItem(handle)        
            handle.show()

    def hide_handles(self):
        for handle in self.handles:
            if handle.isVisible():                
                handle.hide()                     

    def remove_handles(self, scene):
        for handle in self.handles:
            scene.removeItem(handle)
        handles = []

    def select(self):
        self.selected = True
        # print('selecting, changing color from ', self.pen())
        self.set_color(True)
        # self.scene.update()
        # print('after changing color the pen is ', self.pen())

    def deselect(self):                           
        self.selected = False                     
        self.set_color()
        self.hide_handles()
        # return super().clearSelection(self)

    def clean(self, scene):                              
        remove_handles()
        scene.removeItem(self)

    def move(self):
        self.setTransformOriginPoint(xc, yc)

    def rotate(self):
        self.setRotation(np.rad2deg(theta))

    # def scale(self):
    def set_color(self, selected=False):
        if selected:
            col= '#FFFF00' #TODO fix selected color not updating
            # self.setBrush(QBrush(QColor(col))) Brush seems to appear if outline was empty
            # but doesn't change afterwards
        else:
            col = get_color(self.outline_id)

        self.pen.setColor(QColor(col))

    # def set_selectable(self):
    #     if not QGraphicsItem.ItemIsSelectable & self.flags():
    #         self.setFlags(QGraphicsItem.ItemIsSelectable)

    # def set_unselectable(self):
    #     if QGraphicsItem.ItemIsSelectable & self.flags():
    #         pass
    #         # self.setFlags(~QGraphicsItem.ItemIsSelectable) TODO: Fix set_unselectable.
    #         # It's erasing the outline for some reason

    def set_polygon(self):
        self.setPolygon(self.poly)
        size_sum = np.sum(self.poly.boundingRect().size().toTuple())
        self.pen = penItem(0.01 * size_sum) #Width depends of size of cell
        self.set_color()
        self.setPen(self.pen)

        if self not in self.canvas.myscene.items():
            self.canvas.myscene.addItem(self)

    def itemChange(self, change, value):
        pass
        if change == QGraphicsItem.ItemSelectedChange:
            if value == True:
                pass
                # print('selected')
                # self.selected = True

            else:
                pass
                # print('deselected')
                # self.deselect()
                # self.signals.deselected.emit()

        return QGraphicsItem.itemChange(self, change, value)

    def to_ndarray(self, size, scale=4):
        # Convert outline to a numpy array of a defined size.
        array = np.zeros(size)
        for x,y in self.fitted_points:
            x = x/scale # rescale the values to the original image's size
            y = y/scale
            if x < size[0]-1 and y < size[1]-1:
                array[int(np.round(x)), int(np.round(y))] = 1

        return(array)
        
class BSpline(Outline):
    def __init__(self, outline_id, canvas=None,
                 centre=None, points=None, handles=None, nhandles=None):
        super().__init__(outline_id=outline_id, canvas = canvas,
                         points=points)


        if handles is not None: # TODO Add points fitting                                      
            self.set_handles(handles)

        elif centre is not None:
            self.nhandles = 4 if nhandles is None else nhandles

            def_rad = 20
            handle_pos = np.array([[np.cos(theta), np.sin(theta)] for theta \
                          in np.linspace(np.pi*2/self.nhandles,
                                         2 * np.pi, num=self.nhandles)])
            self.centre = np.asarray(centre)
            self.set_handles(self.centre  + def_rad * handle_pos)
        self.fit_spline()

    def fit_spline(self):
        tofit = [(h.orig_x + h.x(), h.orig_y + h.y()) for h in self.handles]
        self.fitted_points = bspline(tofit)
        self.poly = QPolygonF([QPointF(x, y) for x, y in self.fitted_points])

def get_color(n):
    # get color from integer following a discrete palette
    cmap = plt.get_cmap('Set2').colors
    hex_list = [colors.rgb2hex(i) for i in cmap]

    return hex_list[n % len(hex_list)]


