#!/usr/bin/env python

import os
from copy import copy
import numpy as np
from scipy.ndimage import binary_fill_holes

from PySide2 import QtWidgets
from PySide2.QtCore import Qt, Slot, Signal, QObject
from PySide2.QtWidgets import QFrame, QGridLayout

from image_io import load_image
from image_processing import ndarray_to_png
from canvas import Canvas, Handle, BSpline

#Temporal images to test basic features
IMG_FOLDER = "../test/data/"

GFP_IMG = os.path.join(IMG_FOLDER, "pos001_trap003_tp0002_GFP.png")
OUTLINES = os.path.join(IMG_FOLDER, "pos001_trap003_tp0002_segoutlines.png")

#TODO add grouping
class canvasManager(QtWidgets.QFrame):
    def __init__(self,
                 parent=None,
                 shown_images=[None],
                 n_images=1,
                 min_width=117,
                 min_height=117,
                 glob_scale=4):
        super().__init__()

        self.nstacks = 1
        self.width = min_width
        self.height = min_height
        self.glob_scale = glob_scale
        # self.ncells = None
        self.channels = ['GFP']

        self.cols = ['stacks']
        self.rows = ['channels', 'clips']
        self.clipped_channels= [] #['GFP']

        self.ncategs = {
            'stacks': self.nstacks,
            'clips': len(self.clipped_channels),
            'channels': len(self.channels)
        }

        # selection
        self.selectable_modes = ['Handle', 'Select']
        self.cell = 0
        self.outlines = []
        self.max_id = 0

        self.update_layout(['stacks', 'cells', 'channels'])
        self.signals = canvasManagerSignals()
        self.connect_signals()

        # outline storage
        self.storage = {}
        self.location = None
        
    def get_outline_cache(self, location):
        return self.storage.get(location, [])

    def load_images_and_outlines(self, location):
        outlines = self.storage[location]

    @Slot(list, list) # Receive the images to update
    def load_images(self, location, imgs):
        self.meta = True if len(location)>1 else False # pass this from location mgr

        # Clean current outline
        if self.outlines:
            self.storage[str(self.location)] = self.outlines

            o_ids = [o.outline_id for o in self.outlines]
            self.remove_outlines(o_ids, None)
            self.outlines = []

        self.location = location
        self.channels_seen = {}
        
        # read imgs and organise in a dict
        if self.meta:
            for fname in imgs:
                img, info = load_image(fname)
                channel= info['channel']
                if channel in self.channels or channel == 'segoutlines':
                    self.channels_seen[channel] = img

            split= {}
            for channel, imglist in self.channels_seen.items():
                if channel != 'segoutlines':
                    if 'stacks' in self.cols or self.rows:
                        split[channel] = [imglist[..., i] for i in range(self.nstacks)]
                    else:
                        split[channel] = [imglist[..., int(np.ceil(self.nstacks/2))]]

            # if self.clipped_channels:
            #     seg = self.channels_seen['segoutlines']
            #     seg = np.dstack([
            #         binary_fill_holes(seg[..., i]) for i in range(seg.shape[2])
            #     ]).astype(bool)
            #     for channel in self.clipped_channels:
            #         if channel != 'segoutlines':
            #             for stack in range(self.nstacks):
            #                 tmp = copy(split[channel][stack])
            #                 np.place(tmp, ~seg[..., self.cell], 0)
            #                 split[channel].append(tmp) 

            self.imglist = [img for group in split.values() for img in group]

            # add outlines if existing TODO find a better place to put this and fix it
            # self.outlines = self.get_outline_cache(str(location))
            # for outline in self.outlines:
            #     self.add_and_connect(outline)
            #     print('adding outline ', outline)
        else:
            self.imglist = [load_image(img, meta=False)[0] for img in imgs]

        self.width, self.height = self.imglist[0].shape
        self.send_images()


    def send_images(self):
        for i, img in enumerate(self.imglist):
            print("Updating to image ".format(img))
            self.canvases[i].update_image(img)

    def update_layout(self, shown_categories):

        ncols = 1
        for colname in shown_categories:
            if colname in self.cols:
                ncols *= self.ncategs[colname]

        nrows = 1
        for rowname in shown_categories:
            if rowname in self.rows:
                nrows *= self.ncategs[rowname]

        self.canvases = []

        self.layout = QGridLayout()
        categ_dict = {categ: -1 for categ in self.ncategs.keys()}
        nrow = -1
        i = 0
        for rowname in self.rows:
            for row_index in range(self.ncategs[rowname]):
                nrow += 1
                categ_dict[rowname] = row_index
                ncol = 0
                for colname in self.cols:
                    for col_index in range(self.ncategs[colname]):
                        categ_dict[colname] = col_index
                        self.canvases.append(Canvas(i, categ_dict))
                        self.layout.addWidget(self.canvases[-1], nrow, ncol, 1,
                                              1)
                        ncol += 1
                        i += 1

        self.setLayout(self.layout)

    def connect_signals(self):
        for canvas in self.canvases:
            # canvas.signals.updated_outline.connect(self.update_outline)
            canvas.signals.handles_drawn.connect(self.ask_handle_mode)
            # canvas.signals.new_selection.connect(self.sel_changed)
            canvas.signals.new_outline.connect(self.add_outline)
            canvas.signals.removed_outlines.connect(self.remove_outlines)

    def save_current_outlines(self, outfile):
        # print("Saving outlines.")
        if self.outlines:
            outlines = [outline.to_ndarray((
                self.width, self.height), scale=self.glob_scale) \
                        for outline in self.outlines]
            out_ndarray = np.concatenate(outlines, axis=1)
        else:
            out_ndarray = np.zeros((self.width, self.height))
                    
        out_png = ndarray_to_png(out_ndarray).save(outfile)

    @Slot(int)
    def sel_changed(self, outline_id):
        # self.deselect_all()
        if outline_id is not None:
            pass # select all and show handles
        
        # self.outlines[outline_id].setSelected(True)
            
    # def set_selection(self, outline_id):
    #     # self.deselect_all()
    #     # self.canvas_selected = canvas_id
    #     # draw_handles = False if len(outline_ids) > 1 else True
    #     for i, canvas in enumerate(self.canvases):
    #             pass
    #         # TODO Change color and manual selection (internal var)
    #             # canvas.set_selection(outline_id)
    #         else:
    #             for outline_id in outline_ids:
    #                 canvas.set_selection(outline_id, draw_handles=draw_handles)
                
    def deselect_all(self):
        for outline in self.outlines:
            outline.setSelected(False)
            #     outline.deselect()
           
    @Slot()
    def ask_handle_mode(self):
        print('asking for handle mode')
        self.signals.ask_for_emode.emit('Handle')

    @Slot(str)
    def set_edit_mode(self, edit_mode):
        for canvas in self.canvases:
            canvas.set_edit_mode(edit_mode)
        # if edit_mode not in self.selectable_modes:
        #     self.make_unselectable()
        

    def make_selectables(self):
        for canvas in self.canvases:
            for outline in canvas.outlines:
                outline.set_selectable()

    def make_unselectable(self):
        print('making unselectables')
        self.deselect_all()
        for canvas in self.canvases:
            for outline in canvas.outlines:
                outline.set_unselectable()
    @Slot(str)
    def set_outline_mode(self, outline_mode):
        for canvas in self.canvases:
            canvas.set_outline_mode(outline_mode)

    @Slot(float, float)
    def add_outline(self, x=None, y=None):
        # if self.outline_mode == 'BSpline':
        nid = self.max_id+1
        self.add_and_connect(BSpline(outline_id=nid, canvas=None, centre=[x, y]))

        # fit and adjust numbers
        self.max_id += 1
        nindex = self.find_index(nid)
        # self.outlines[nindex].fit_spline()

        self.set_edit_mode('Handle')
        # outline.setSelected(True) select all
         
    def add_and_connect(self, outline):
        self.outlines.append(outline)

        for canvas in self.canvases:
            canvas.outlines.append(copy_outline(outline, canvas))
            canvas.outlines[-1].set_polygon()

            # Connect
            for handle in canvas.outlines[-1].handles:
                handle.signals.pos_changed.connect(canvas.update_polygon)
                handle.signals.pos_changed.connect(self.sync_handles)

    @Slot(list)
    def update_position(self):
        print('updating position')
        pass

    @Slot(list, int)
    def remove_outlines(self, o_ids, c_id):
        for canvas in self.canvases:
            for o_id in o_ids:
                idx = canvas.find_index(o_id)
                # if canvas.canvas_id != c_id:
                #     canvas.signals.blockSignals(True) # block signals to prevent recursion
                #     canvas.outlines[idx].setSelected(True)
                #     canvas.signals.blockSignals(False)
                del canvas.outlines[idx]

        del self.outlines[self.find_index(o_id)]
                
    @Slot(int, int, int, float, float)
    def sync_handles(self, c_id, o_id, h_id, dx, dy):
        idx = self.find_index(o_id)
        for canvas in self.canvases:
            if canvas.canvas_id != c_id:
                canvas.outlines[idx].handles[h_id].signals.blockSignals(True)
                canvas.outlines[idx].handles[h_id].setPos(dx, dy)
                canvas.outlines[idx].handles[h_id].signals.blockSignals(False)
                canvas.outlines[idx].fit_spline()
                canvas.outlines[idx].set_polygon()

        self.outlines[idx].fitted_points = canvas.outlines[idx].fitted_points


        
    def find_index(self, o_id):
        return next((i for i, o in enumerate(self.outlines)
                     if o.outline_id == o_id), None)

    def select_outline(self, outline_id):
        index = self.find_index(outline_id) if outline_id >= 0 else len(self.outlines)

        assert index is not None, 'outline_id is not in outlines array'

        self.outlines[index].setSelected(True)
        

    def set_selection(self, outline_id, draw_handles=False):
        # Draws the outline of selection and optionally the handles too
        self.selected_outline = outline_id
        if draw_handles:
            self.signals.handles_drawn.emit()
            # self.outlines[outline_id].show_handles()

        # for handle in outline.handles:
            handle.signals.pos_changed.connect(outline.update_polygon)


    # @Slot(int, int)
    # def update_outline(self, canvas_id, outline_id):
    #     new_outline = self.canvases[canvas_id].outlines[outline_id]
    #     # Update outline in manager (not being used atm)
    #     if outline_id in [o.outline_id for o in self.outlines]:
    #         # new_outline = copy_outline(self.outlines[outline_id])
    #         self.outlines[outline_id] = new_outline
    #     else:
    #         self.outlines.append(new_outline)

    #     for i, canvas in enumerate(self.canvases):
    #         new_outline = copy_outline(self.outlines[outline_id])
    #         # canvas.add_and_connect(new_outline)
    #         canvas.myscene.addItem(new_outline)

class canvasManagerSignals(QObject):
    ask_for_emode = Signal(str)

class pathItem(QtWidgets.QGraphicsPathItem):
    def __init__(self, scale):
        super().__init__()
        pen = QtGui.QPen()
        pen.setColor('#5ebb49')
        pen.setWidth(1 * scale)
        self.setPen(pen)


def copy_outline(outline, canvas):
    handles = [(h.orig_x, h.orig_y) for h in outline.handles]
    new_outline= BSpline(outline.outline_id, canvas=canvas, handles=handles)
    new_outline.fit_spline()

    return new_outline
