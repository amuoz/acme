'''
Functions that are purely for geometry of images. They don't require but numpy and skimage
'''
import numpy as np
import scipy.interpolate as si

import numpy as np
from skimage.measure import EllipseModel


def get_handle_loc(outline_mode, params, i):
    if outline_mode == 'Ellipse':  #TODO: Convert these into methods for an outline class
        xc, yc, a, b, theta = params
        perp = np.deg2rad(90)
        a_x = a * np.cos(0) * np.cos(theta) - b * np.sin(0) * np.sin(theta)
        a_y = a * np.cos(0) * np.sin(theta) + b * np.sin(0) * np.cos(theta)
        b_x = a * np.cos(perp) * np.cos(theta) - b * np.sin(perp) * np.sin(
            theta)
        b_y = a * np.cos(perp) * np.sin(theta) + b * np.sin(perp) * np.cos(
            theta)
        cases = {
            0: (xc, yc),
            1: (xc + a_x + b_x, yc + a_y + b_y),
            2: (xc + b_x, yc + b_y)
        }
        
    return cases[i]


def get_new_params(xy, outline_mode, params, handle_id):
    if outline_mode == 'Ellipse':
        new_x, new_y = xy
        xc, yc, a, b, theta = params
        theta = np.rad2deg(theta)
        orig_x, orig_y = get_handle_loc(outline_mode, params, handle_id)
        rotation = np.rad2deg(np.arctan2(new_y - orig_y, new_x - orig_x))
        if handle_id == 0:
            params[0] += new_x
            params[1] += new_y
        elif handle_id == 1:
            sign = 1 if new_x > 0 else -1
            params[4] += sign * np.deg2rad(rotation) / 40
        elif handle_id == 2:
            params[2] += new_x
            params[3] += new_y

    return params


def get_handle_color(handle_id):
    cases = {0: 'red', 1: 'teal'}

    return cases.get(handle_id, 'blue')


def fit_points(outline_mode, points):
    if outline_mode == 'Ellipse':
        params = fit_ellipse(points)
    elif outline_mode == 'spline':
        params = fit_spline(points)
    return params


def fit_spline(points):
    from scipy.interpolate import splprep


def fit_ellipse(points):
    '''
    Use skimage to fit an ellipse.
    --
    input
    :points: list of tuples with points

    output
    xcenter, ycenter, long axis, short axis, rotation angle
    '''
    ell = EllipseModel()
    ell.estimate(np.array(points))

    return (ell.params)


def get_centroid(arr):
    length = arr.shape[0]
    sum_x = np.sum(arr[:, 0])
    sum_y = np.sum(arr[:, 1])
    return [np.round(i) for i in (sum_x / length, sum_y / length)]


def bspline(cv, n=500, degree=3, periodic=True):
    """ Calculate n samples on a bspline

        cv :      Array ov control vertices
        n  :      Number of samples to return
        degree:   Curve degree
        periodic: True - Curve is closed
                  False - Curve is open

    Obtained from https://stackoverflow.com/a/35007804
    """

    # If periodic, extend the point array by count+degree+1
    cv = np.asarray(cv)
    count = len(cv)

    if periodic:
        factor, fraction = divmod(count+degree+1, count)
        cv = np.concatenate((cv,) * factor + (cv[:fraction],))
        count = len(cv)
        degree = np.clip(degree,1,degree)

    # If opened, prevent degree from exceeding count-1
    else:
        degree = np.clip(degree,1,count-1)


    # Calculate knot vector
    kv = None
    if periodic:
        kv = np.arange(0-degree,count+degree+degree-1)
    else:
        kv = np.clip(np.arange(count+degree+1)-degree,0,count-degree)

    # Calculate query range
    u = np.linspace(periodic,(count-degree),n)


    # Calculate result
    return np.array(si.splev(u, (kv,cv.T,degree))).T
