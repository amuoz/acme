import os
import numpy as np

from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtWidgets import (QLabel, QGridLayout, QListWidgetItem,
                               QListWidget)
from PySide2.QtCore import QObject, Signal, Slot  #, QThread

from metadata2df import generate_position_list_from_folder


class localSignals(QObject):
    signal_list = Signal(list)
    signal_int_str = Signal(int, str)
    signal_str = Signal(str)
    send_pos_and_images = Signal(list, list)
    pos_set_loaded = Signal()


class PosController(QObject):
    def __init__(self, df=None):
        self.df = df
        self.location_frame = locationFrame()
        self.signals = localSignals()

    @Slot(list)
    def init_location_frame(self, fields):
        self.meta = True if len(fields) > 1 else False
        self.location_frame.set_fields(fields)
        self.selection_index = [
            0 for internal_widget in self.location_frame.internal_widgets
        ]
        self.init_controller()

    def init_controller(self):
        self.location_frame.set_list_widgets()
        self.connect_signals()
        self.location_frame.set_layout()

    @Slot(int, str)
    def receive_changed_selection(self, new_index, metadata_field):
        metadata_index = self.metadata_fields.index(metadata_field)

        # Update index
        self.selection_index[metadata_index] = new_index
        for i in range(metadata_index + 1, len(self.metadata_fields)):
            self.selection_index[i] = 0

        new_index = self.selection_index
        # self.set_selection()
        self.update_location(new_index, metadata_field)


    @Slot(str)
    def load_input(self, arg_str):
        # Load a prediction dataframe if the file is a csv, else load whole folder
        self.df = pd.read_csv(arg_str, index_col=0) if arg_str.endswith('.csv') else \
            generate_position_list_from_folder(arg_str)

        assert len(self.df.index) > 0, "No image found in folder"

        # set the metadata fields in the position control manager
        self.metadata_fields = ['position', 'trap', 'tp'] if \
            len(self.df.columns) > 1 else ['filename']
        self.init_location_frame(self.metadata_fields)

        self.fill_frame()

    def fill_frame(self):
        self.set_sublists()
        self.set_selection()
        # Set the new sublists as zero
        self.update_location([0 for field in self.metadata_fields],
                             self.metadata_fields[0])

    def set_selection(self):
        self.selection = []
        for i, internal_widget in enumerate(
                self.location_frame.internal_widgets):
            internal_widget.selection_index = internal_widget.setItemSelected(
                internal_widget.item(self.selection_index[i]), 1)
            # TODO: Fix this to be ints and not QItems
            self.selection.append(
                self.location_frame.sublists[i][self.selection_index[i]])

    def set_sublists(self):
        '''
        We consecutively filter of our location dataframe using the selected values
        '''
        visual_sublists = [np.sort(self.df[self.metadata_fields[0]].unique())]
        sub_dfs = [
            self.df.loc[self.df[self.metadata_fields[0]] == visual_sublists[0][
                self.selection_index[0]]]
        ]
        for field, val in zip(self.metadata_fields[1:],
                              self.selection_index[1:]):
            unique_values = np.sort(sub_dfs[-1][field].unique())
            visual_sublists.append(unique_values)

            sub_df = sub_dfs[-1].loc[sub_dfs[-1][field] == unique_values[val]]
            sub_dfs.append(sub_df)

        # convert to int values if possible, otherwise to strings
        self.location_frame.sublists = []
        for sublist in visual_sublists:
            self.location_frame.sublists.append([])
            for item in sublist:
                try:
                    conv_item = int(item) 
                except:
                    conv_item = str(item)

                self.location_frame.sublists[-1].append(conv_item)

    def update_location(self, new_index, metadata_field):
        # Block signals for list change
        for internal_widget in self.location_frame.internal_widgets:
            internal_widget.blockSignals(True)

        # Update lists, models and selection
        self.set_sublists()
        self.location_frame.update_models(metadata_field, new_index)
        self.selection = [
            self.location_frame.sublists[field_index][element_index]
            for field_index, element_index in enumerate(new_index)
        ]
        # Unblock signals after updating selection and list
        for internal_widget in self.location_frame.internal_widgets:
            internal_widget.blockSignals(False)

        self.emit_location_list()

    def emit_location_list(self):
        '''
        Sends the current position for the imageController class to catch and change the image
        '''
        self.signals.signal_list.emit(self.selection)
        if self.meta:
            self.signals.send_pos_and_images.emit(self.selection,
                                              self.get_filenames(self.selection))
        else:
            self.signals.send_pos_and_images.emit(self.selection,
                                                  self.selection)
            

    def connect_signals(self):
        #Connect signals to slots
        for internal_widget in self.location_frame.internal_widgets:
            internal_widget.currentRowChanged.connect(
                internal_widget.emit_current_selection)
            internal_widget.signals.signal_int_str.connect(
                self.receive_changed_selection)

    def update_sublists(self):
        self.location_frame = locationFrame(
            metadata_fields=self.metadata_fields)

    @Slot(str)
    def select_next_value(self, field_to_change):
        metadata_index = self.metadata_fields.index(field_to_change)
        # Find the index for the current selected value
        local_index = self.location_frame.sublists[metadata_index].index(
            self.selection[metadata_index])
        if metadata_index >= 0:
            if local_index < len(
                    self.location_frame.sublists[metadata_index]) - 1:
                # Update selection and their index
                self.selection_index[metadata_index] = local_index + 1
                self.selection[metadata_index] = self.location_frame.sublists[
                    metadata_index][self.selection_index[metadata_index]]

                widget_to_change = self.location_frame.internal_widgets[
                    metadata_index]
                widget_to_change.setItemSelected(
                    widget_to_change.item(local_index + 1), 1)
                self.update_location(self.selection_index, field_to_change)
            else:
                # print("Changing " + self.metadata_fields[metadata_index])
                if metadata_index > 0:
                    self.selection_index[metadata_index] = 0
                    self.selection[
                        metadata_index] = self.location_frame.sublists[
                            metadata_index][0]

                    widget_to_change = self.location_frame.internal_widgets[
                        metadata_index]
                    widget_to_change.setItemSelected(widget_to_change.item(0),
                                                     1)

                    field_to_change = self.metadata_fields[metadata_index - 1]
                    self.select_next_value(field_to_change)
                else:
                    print("We are in the endgame now")
            # print(self.selection_index)

    def get_filenames(self, pos):
        potential_fnames = ['filename', 'filename_seg', 'filename_GFP']
        outcols = [name for name in potential_fnames if name in self.df.columns]
        out_df = self.df
        for col, val in zip(self.metadata_fields, pos):
            out_df = out_df.loc[out_df[col]==val]
            

        result= np.unique(out_df[outcols].values)
        return result.tolist()

class locationFrame(QtWidgets.QGroupBox):
    def __init__(self):
        super().__init__()
        self.setTitle("Image location")


    # def init_placeholder(self):
    #     self.metadata_fields = []
    #     self.set_layout()

    def set_fields(self, metadata_fields):
        '''
        input:
        :metadata_fields: list of strings corresponding to the
        location hierarchy

        output:
        Defines
        :self.metadata_fields: list of str indicating the metadata fields
        :self.internal_widgets: list of QWidgets containing the fields
        '''
        self.sublists = [[''] for field in metadata_fields]
        self.metadata_fields = metadata_fields
        self.internal_widgets = [None for i in range(len(metadata_fields))]
        
    def set_layout(self):
        '''
        Requires:
        - self.metadata_fields
        - self.internal_widgets
        '''
        self.layout = QGridLayout()
        for i, field in enumerate(self.metadata_fields):
            self.layout.addWidget(QLabel(field), 0, i, 1, 1)
            self.layout.addWidget(self.internal_widgets[i], 1, i, 1, 1)
        self.setLayout(self.layout)

    def set_list_widgets(self):
        '''Sets the internal widgets at startup.
        requires:
        self.metadata_fields
        self.sublists
        '''
        for i, (field,
                sublist) in enumerate(zip(self.metadata_fields,
                                          self.sublists)):
            self.internal_widgets[i] = listView(name=field, vals=sublist)

    def update_models(self, metadata_field, new_indices):
        ''' Update models for all sublists with information
        self: QGroupBox containing the lists with locations
        metadata_field:
        new_indices:
        '''
        metadata_idx = self.metadata_fields.index(metadata_field)

        # We only update the lists downstream in the hierarchy
        for i, (internal_widget, new_idx) in enumerate(
                zip(self.internal_widgets[metadata_idx:],
                    new_indices[metadata_idx:])):
            internal_widget.vals = self.sublists[metadata_idx + i]
            internal_widget.selected_idx = new_idx if i == 0 else 0
            internal_widget.update_model_and_view()


class listView(QListWidget):
    def __init__(self, selected_idx=0, name=None, vals=None):
        super().__init__()

        self.name = name
        self.vals = vals
        self.selected_idx = selected_idx

        self.signals = localSignals()

        self.init_widget()

    def init_widget(self):
        self.generate_data()

    def generate_data(self):
        for val in self.vals:
            item = QListWidgetItem(str(val))
            self.addItem(item)
        self.setCurrentRow(self.selected_idx)

    def update_model_and_view(self):
        self.clear()
        self.generate_data()
        for index, value in enumerate(self.vals):
            item = self.item(index)
            item.setText(str(value))
        self.setCurrentRow(self.selected_idx)
        self.update()

    def emit_current_selection(self):
        current_row = int(self.currentRow())
        current_row = current_row if current_row >= 0 else 0
        self.signals.signal_int_str.emit(current_row, self.name)
