#!/usr/bin/env python3
'''
Sources:

'''

import sys
import os
import itertools
import numpy as np
from skimage import io

from fbs_runtime.application_context.PySide2 import ApplicationContext

from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import Qt, Slot, Signal
from PySide2.QtGui import QPen
from PySide2.QtWidgets import QFrame, QGridLayout, QSizePolicy

from canvas_manager import canvasManager
from control_menu import menuFrame
from message_box import messageBox


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        # frame_rect = app.desktop().frameGeometry()
        # width, height = (800, 600)
        # self.resize(width, height)

        w = QtWidgets.QWidget()
        self.canvas_manager = canvasManager()
        l = QtWidgets.QGridLayout()
        l.addWidget(self.canvas_manager, 0, 0, 1, 1)

        self.menu_frame = menuFrame()
        l.addWidget(self.menu_frame, 0, 1, 2, 1)

        thinbar_layout = QtWidgets.QHBoxLayout()
        self.msg_box = messageBox()
        thinbar_layout.addWidget(self.msg_box)
        thinbar_layout.addStretch()
        l.addLayout(thinbar_layout, 1, 0, 1, 1)

        self.connect_signals()
        self.initialise_menu_options()

        w.setLayout(l)
        self.setCentralWidget(w)

    def connect_signals(self):
        # This are the outermost signals that connect the submodules
        # Message display
        for slot in [self.canvas_manager.set_edit_mode, self.msg_box.mode_changed_msg]:
            self.menu_frame.tabs_holder.tabs['Annotation'].emode_list.currentTextChanged.connect(
                slot)


        # Outline mode
        for slot in [self.canvas_manager.set_outline_mode, self.msg_box.mode_changed_msg]:
            self.menu_frame.tabs_holder.tabs['Annotation'].omode_box.currentTextChanged.connect(
                slot)

        self.menu_frame.tabs_holder.tabs['Annotation'].omode_box.currentTextChanged.connect(
            self.msg_box.mode_changed_msg)


        # Edition mode
        for canvas in self.canvas_manager.canvases:
            canvas.signals.edit_mode_changed.connect(
                self.menu_frame.tabs_holder.tabs['Annotation'].emode_list.set_edit_mode)

        self.canvas_manager.signals.ask_for_emode.connect(
            self.menu_frame.tabs_holder.tabs['Annotation'].emode_list.set_edit_mode)

        # Quick annotation
        self.menu_frame.tabs_holder.tabs['Annotation'].quick.save_next_btn.clicked.connect(
            self.quicksave
        )

        # Image updating and loading
        self.menu_frame.pos_control.signals.send_pos_and_images.connect(
        self.canvas_manager.load_images)

    def initialise_menu_options(self):
        #TODO adjust these for when reading existing outlines
        self.menu_frame.tabs_holder.tabs['Annotation'].emode_list.setCurrentRow(0)
        self.canvas_manager.set_outline_mode(self.menu_frame.tabs_holder.tabs['Annotation'].omode_box.currentText())

        # Load first tp
        pos_control = self.menu_frame.pos_control
        if hasattr(pos_control, 'selection'):
            pos_control.signals.send_pos_and_images.emit(pos_control.selection,
                                              pos_control.get_filenames(pos_control.selection))

    def quicksave(self):
        io = self.menu_frame.tabs_holder.tabs['I/O']
        outdir = io.input_dir[0]

        pos_control = self.menu_frame.pos_control
        a = pos_control.get_filenames(pos_control.selection)
        fname = os.path.commonprefix(a)
        fname = fname.split('.')[0]

        outname = os.path.abspath(fname + '_' + io.annot_name.text() + '.png')
        self.canvas_manager.save_current_outlines(outname)
        self.menu_frame.pos_control.select_next_value(
            self.menu_frame.pos_control.metadata_fields[-1])
            
appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
window = MainWindow()
window.show()
exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
sys.exit(exit_code)

