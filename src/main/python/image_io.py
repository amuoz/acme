import json
import numpy as np
from imageio import imread


def load_tiled_image(filename):
    print('reading file', filename)
    tImg = imread(filename)
    info = json.loads(tImg.meta.get('Description', '{}'))
    tw, th = info.get('tilesize', tImg.shape[0:2])
    nt = info.get('ntiles', 1)
    nr, nc = info.get('layout', (1, 1))
    nc_final_row = np.mod(nt, nc);
    img = np.zeros((tw, th, nt), dtype=tImg.dtype)
    for i in range(nr):
        i_nc = nc_final_row if i+1==nr and nc_final_row>0 else nc
        for j in range(i_nc):
            ind = i*nc+j
            img[:,:,ind] = tImg[i*tw:(i+1)*tw,j*th:(j+1)*th];
    return img, info

def load_image(filename, meta=True):
    # Handles the import of tiled images or raw ones
    if meta:
        return load_tiled_image(filename)
    else:
        return (imread(filename), None)


def normalise_array(ndarray, mask=None):
    #To cover for masked/unmasked arrays
    ndarrayData = ndarray[~ndarray.mask].data if isinstance(ndarray, np.ma.MaskedArray) else ndarray
    if mask is not None:
        np.place(ndarrayData, ~mask, 0)

    linearisedData = np.ravel(ndarrayData[ndarrayData>0])

    # Perform simple normalization
    assert np.sum(linearisedData) > 0, "Found empty array"
    maxInt= linearisedData.max()
    minInt= linearisedData.min()

    conversionDict={}
    for val in linearisedData:
        conversionDict[val]=int(np.round(255* ((val-minInt)/(maxInt-minInt))))

    f = lambda x: conversionDict[x] if x in conversionDict.keys()  else 0
    vectorizedFun = np.vectorize(f)
    result=vectorizedFun(ndarray)

    # If it is a maskedArray, return the whole image with the masked part as 0's
    whole_result = result.filled(0) if isinstance(ndarray, np.ma.MaskedArray) else result

    return whole_result

from skimage.io._plugins.pil_plugin import ndarray_to_pil
